const express = require('express');

const router = express.Router();
const { authentification } = require('./authentification');
const {
  createNote, getNote, getNotes, updateNote, checkNote, deleteNote,
} = require('./noteActions');

router.post('/api/notes', authentification, createNote);
router.get('/api/notes', authentification, getNotes);
router.get('/api/notes/:id', authentification, getNote);
router.put('/api/notes/:id', authentification, updateNote);
router.patch('/api/notes/:id', authentification, checkNote);
router.delete('/api/notes/:id', authentification, deleteNote);

module.exports = {
  NotesRouter: router,
};
