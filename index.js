const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

const PORT = 8080;

const { UserRouter } = require('./UserRouter');
const { NotesRouter } = require('./NotesRouter');

app.use(express.json());
app.use(morgan('tiny'));
app.use(UserRouter, NotesRouter);

mongoose.connect('mongodb+srv://pafa07:0A1b2c3d4e@olegpafacluster.izujzgt.mongodb.net/todoapp?retryWrites=true&w=majority');

app.listen(PORT);

function errorHandler(err, req, res) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}

app.use(errorHandler);
