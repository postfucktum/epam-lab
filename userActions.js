const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { Person } = require('./Users');

const createdDate = new Date();

const creatingUser = async (req, res, next) => {
  const { name, username, password } = req.body;

  const user = new Person({
    name,
    username,
    password: await bcrypt.hash(password, 10),
    createdDate,
  });

  user.save()
    .then(() => res.status(200).json({ message: 'Success' }))
    .catch(() => {
      if(res.status === 400 || res.status === 500){
        next({ message: 'string' });
      }
      
    });
};

const loginUser = async (req, res) => {
  const user = await Person.findOne({ username: req.body.username });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { username: user.username, name: user.name, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res.status(200).json({ message: 'Success', jwt_token: jwtToken });
  }
  if(res.status === 400 || res.status === 500){
    return res.json({ message: 'string' });
  } 
  
};

const viewProfile = (req, res, next) => {
  Person.find().then(() => {
    res.status(200).json({ _id: req.user.userId, username: req.user.username, createdDate: new Date() });
  })
    .catch(() => {
      if(res.status === 400 || res.status === 500){
        next({ message: 'string' });
      }
      
    });

};

const deleteUser = (req, res, next) => {
  Person.findByIdAndDelete(req.user.userId)
    .then(() => {
      res.status(200).json({ message: 'Success' });
    }).catch(() => {
      if(res.status === 400 || res.status === 500){
        next({ message: 'string' });
      }
      
    });
};

const changePass = async (req, res, next) => {
  const newPass = await bcrypt.hash(req.body.newPassword, 10);
  const oldPass = req.body.oldPassword;
  console.log(newPass, oldPass);
  Person.findByIdAndUpdate({ _id: req.user.userId }, { $set: { password: newPass } })
    .then(() => res.status(200).json({ message: 'Success' }))
    .catch(() => {
      if(res.status === 400 || res.status === 500){
        return res.json({message: "string"})
      }
      
    });
};

module.exports = {
  creatingUser,
  loginUser,
  viewProfile,
  deleteUser,
  changePass,
};
