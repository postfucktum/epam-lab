const { Note } = require('./Notes');

function createNote(req, res) {
  const { text, createdDate = new Date() } = req.body;
  const note = new Note({
    completed: true,
    text,
    userId: req.user.userId,
    createdDate,
  });
  note.save().then(() => {
    res.status(200).json({ message: 'Success' });
  }).catch(() => {
    if(res.status === 400 || res.status === 500){
       return res.json({ message: 'string' });
    }
    
  });
}

const getNote = (req, res) => {
  Note.findById(req.params.id).then((note) => res.status(200).json({ note }))
    .catch(() => {
        if(res.status === 400 || res.status === 500){
            return res.json({ message: 'string' });
       }
    });
};

const getNotes = (req, res) => {
  Note.find().then((result) => res.status(200).json({
    offset: req.query.offset,
    limit: req.query.limit,
    count: result.note.length,
    notes: result,
  })).catch(() => {
    if(res.status === 400 || res.status === 500){
        return res.json({ message: 'string' });
   }
});
};

const updateNote = (req, res) => {
  Note.findByIdAndUpdate(req.params.id, { $set: { text: req.body.text } })
    .then(() => res.status(200).json({ message: 'Success' }))
    .catch(() => {
        if(res.status === 400 || res.status === 500){
            return res.json({ message: 'string' });
        }
    });
};

const checkNote = async (req, res) => {
  const note = await Note.findById(req.params.id);
  note.completed = !note.completed;
  note.save().then(() => res.status(200).json({ message: 'Success' })).catch(() => {
    if(res.status === 400 || res.status === 500){
        return res.json({ message: 'string' });
    }
});
};

const deleteNote = (req, res) => {
  Note.findByIdAndDelete(req.params.id)
    .then(() => res.status(200).json({ message: 'Success' }))
    .catch(() => {
        if(res.status === 400 || res.status === 500){
            return res.json({ message: 'string' });
        }
    });
};

module.exports = {
  createNote,
  getNotes,
  getNote,
  updateNote,
  checkNote,
  deleteNote,
};
