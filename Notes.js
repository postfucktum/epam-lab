const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  completed: {
    type: Boolean,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  createdDate: {
    type: Date,
    required: true,
  },
});

const Note = mongoose.model('notes', noteSchema);

module.exports = {
  Note,
};
