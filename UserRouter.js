const express = require('express');

const router = express.Router();

const {
  creatingUser, loginUser, viewProfile, deleteUser, changePass,
} = require('./userActions');
const { authentification } = require('./authentification');

router.post('/api/auth/register', creatingUser);
router.post('/api/auth/login', loginUser);
router.get('/api/users/me', authentification, viewProfile);
router.delete('/api/users/me', authentification, deleteUser);
router.patch('/api/users/me', authentification, changePass);

module.exports = {
  UserRouter: router,
};
